import re
from typing import Optional, Union

from pydantic import BaseModel, Field, validator


class RequiredBlank(BaseModel):
    class Config:
        allow_population_by_field_name = True

    recipient: str = Field(..., alias="Name")
    bank_account: Union[str, int] = Field(..., alias="PersonalAcc")
    bank_name: str = Field(..., alias="BankName")
    bic: Union[str, int] = Field(..., alias="BIC")
    correspondent: Union[str, int] = Field(..., alias="CorrespAcc")

    @validator("bank_account")
    def validate_bank_account(cls, bank_account):
        bank_account = re.sub(r"[^0-9]", "", str(bank_account))
        return bank_account

    @validator("bic")
    def validate_bic(cls, bic):
        bic = re.sub(r"[^0-9]", "", str(bic))
        return bic

    @validator("correspondent")
    def validate_correspondent(cls, correspondent):
        correspondent = re.sub(r"[^0-9]", "", str(correspondent))
        return correspondent

    def __bytes__(self):
        sep = "|".encode("cp1251")
        blocks = ["ST00011".encode("cp1251")]
        for k, v in self.dict(by_alias=True).items():
            if v is not None:
                blocks.append(f"{k}={v}".encode("cp1251"))
        return sep.join(blocks)

    def __str__(self):
        return self.__bytes__().decode("cp1251")


class BaseBlank(RequiredBlank):
    amount: Optional[int] = Field(None, alias="Sum")
    purpose: Optional[str] = Field(None, alias="Purpose")
    payee_inn: Optional[Union[str, int]] = Field(None, alias="PayeeINN")
    payer_inn: Optional[Union[str, int]] = Field(None, alias="PayerINN")
