from gost56042.blanks.base import BaseBlank, RequiredBlank


def test_required_blank_valid():
    assert (
        str(
            RequiredBlank(
                recipient="Майер Евгений Николаевич",
                bank_account="40817 81076 00000 04077",
                bank_name="Ф Рокетбанк КИВИ Банк (АО)",
                bic="044525420",
                correspondent="30101810945250000420",
            )
        )
        ==
        "ST00011|"
        "Name=Майер Евгений Николаевич|"
        "PersonalAcc=40817810760000004077|"
        "BankName=Ф Рокетбанк КИВИ Банк (АО)|"
        "BIC=044525420|"
        "CorrespAcc=30101810945250000420"
    )


def test_base_blank_valid():
    assert (
        str(
            BaseBlank(
                recipient="Майер Евгений Николаевич",
                bank_account="40817 81076 00000 04077",
                bank_name="Ф Рокетбанк КИВИ Банк (АО)",
                bic="044525420",
                correspondent="30101810945250000420",
                amount=200000,
                purpose="За минет в подъезде. НДС не облагается",
                payer_inn="540823369924"
            )
        )
        ==
        "ST00011|"
        "Name=Майер Евгений Николаевич|"
        "PersonalAcc=40817810760000004077|"
        "BankName=Ф Рокетбанк КИВИ Банк (АО)|"
        "BIC=044525420|"
        "CorrespAcc=30101810945250000420|"
        "Sum=200000|"
        "Purpose=За минет в подъезде. НДС не облагается|"
        "PayerINN=540823369924"
    )
